package com.example.postgres.Controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.example.postgres.konekcije.PostgresKonekcija;
import com.example.postgres.konekcije.MySqlKonekcija;
import com.example.postgres.model.Task;
import com.example.postgres.model.Log;
import com.example.postgres.model.Image;

import com.example.postgres.service.ImageMongoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.http.MediaType;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.UUID;
import java.util.Optional;
import org.springframework.web.multipart.MultipartFile;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.json.*;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.node.*;
import java.lang.Iterable;
import org.bson.types.Binary;
import java.lang.Exception;
/** Represents an RestController.
 * @author Ehvan Gradanin i Enis Ahmetovic i Hamza Iseric
*/

@RestController
public class DefaultController {


    @Autowired
     /** Represents the ImageMongoService.
    */
    private ImageMongoService imageMongoService;
	 
	PostgresKonekcija db = new PostgresKonekcija();
    MySqlKonekcija mydb = new MySqlKonekcija();
	@RequestMapping(value="/openConnection", method = RequestMethod.GET)
    public void openConnection() {
        db.loadDriver();
        db.connect();
        mydb.loadDriver();
    	mydb.connect();
    }
    

	// insert

    @RequestMapping(value="/insertToEnisTable", method = RequestMethod.GET)
    public void insertEnis() {
        db.pripremiInsertToEnisTable();
        db.insertToEnisTable();
 
    }
    @RequestMapping(value="/insertToHamzaTable", method = RequestMethod.GET)
    public void insertHamza() {
        db.pripremiInsertToHamzaTable();
        db.insertToHamzaTable();
 
    }
    @RequestMapping(value="/insertToEhvanTable", method = RequestMethod.GET)
    public void insertEhvan() {
        db.pripremiInsertToEhvanTable();
        db.insertToEhvanTable();
 
    }

    // update

    @RequestMapping(value="/updateEnisTable", method = RequestMethod.GET)
    public void updateEnis() {
        db.pripremiUpdateEnisTable();
        db.updateEnisTable();
 
    }
    @RequestMapping(value="/updateHamzaTable", method = RequestMethod.GET)
    public void updateHamza() {
        db.pripremiUpdateHamzaTable();
        db.updateHamzaTable();
 
    }
    @RequestMapping(value="/updateEhvanTable", method = RequestMethod.GET)
    public void updateEhvan() {
        db.pripremiUpdateEhvanTable();
        db.updateEhvanTable();
 
    }


    // delete

    @RequestMapping(value="/deleteEnisTable", method = RequestMethod.GET)
    public void deleteEnis() {
        db.pripremiDeleteEnisTable();
        db.deleteEnisTable();
 
    }
    @RequestMapping(value="/deleteHamzaTable", method = RequestMethod.GET)
    public void deleteHamza() {
        db.pripremiDeleteHamzaTable();
        db.deleteHamzaTable();
 
    }
    @RequestMapping(value="/deleteEhvanTable", method = RequestMethod.GET)
    public void deleteEhvan() {
        db.pripremiDeleteEhvanTable();
        db.deleteEhvanTable();
 
    }

    @RequestMapping(value="/tasks/{tabela}", method = RequestMethod.GET)
    public ArrayList<Task> getTasks(@PathVariable String tabela) {
    	db.getStatement();
    	return db.getTasks(tabela);
    }

    @RequestMapping(value="/logs", method = RequestMethod.GET)
    public ArrayList<Log> getLogs() {
    	db.getStatement();
    	return db.getLogs();
    }

    @RequestMapping(value="/closeConnection", method = RequestMethod.GET)
    public void closeConnection(){
        db.diskonekt();
        mydb.diskonekt();
    }


    @RequestMapping(value="/getMetaData", method = RequestMethod.GET)
    public JsonNode getMetaData(){
        return db.getMetaData();
    }

    @RequestMapping(value="/migrateDatabase", method = RequestMethod.GET)
    public void migrateDatabase(){
        try{

            mydb.setAutoCommit(false);
            JsonNode metadata = db.getMetaData();




            // 1. kreirati tabele
            String tableCreationSql = "";


           

            ArrayList<String> table_names = new ArrayList<String>();
            ArrayNode array = (ArrayNode)metadata.get("Info o tabelama");
            if (array.isArray()) {
                for (final JsonNode objNode : array) {
                    JsonNode node = objNode.get("Ime Tabela");
                    table_names.add(node.asText(""));

                }
            }

            ObjectNode kolone = (ObjectNode)metadata.get("Kolone");
            for (String table_name : table_names) {
                tableCreationSql = "";
                ArrayNode table = (ArrayNode)kolone.get(table_name);
                tableCreationSql = "Create Table "+ table_name + " (";
                for (final JsonNode kolona : table) {
                    String imeKolone = kolona.get("Ime Kolone").asText("");
                    String tipKolone = kolona.get("Tip Kolone").asText("");
                    if (tipKolone.equals("bpchar")){
                        tipKolone = "varchar";
                    }
                    if (tipKolone.equals("serial")){
                        tipKolone = "int";
                    }
                    int velicinaKolone = kolona.get("Velicina Kolone").asInt();
                    if (imeKolone.equals("id")){
                        tableCreationSql += "id int(" + velicinaKolone + ") auto_increment not null PRIMARY KEY,";   
                    }
                    else if(tipKolone.equals("timestamp")){

                        tipKolone = "timestamp";
                        tableCreationSql += imeKolone + " " + tipKolone + ",";
                    }
                    else{
                        tableCreationSql += imeKolone + " " + tipKolone + "(" + velicinaKolone + "),";
                    }


                }
                tableCreationSql = tableCreationSql.substring(0, tableCreationSql.length() - 1);
                tableCreationSql += ")";
                mydb.executeSql(tableCreationSql);

            }

            // creating users and adding privileges

            ArrayList<String> users = db.getDatabaseUsers();
            ArrayList<String> mysqlUsers = mydb.getDatabaseUsers();
            for (String mysqlUser : mysqlUsers){
                users.remove(mysqlUser);
            }
            users.remove("postgres");
            if(!users.isEmpty()){
                for (String username : users){
                    mydb.createNewDatabaseUser(username);
                }
                for (String table_name : table_names) {
                    try {
                        ResultSet rezultat = db.getUsersPrivileges(table_name);
                        while(rezultat.next()){
                            if(!rezultat.getString("grantee").equals("postgres") && !rezultat.getString("privilege_type").equals("TRUNCATE")){
                                mydb.addPrivileges(rezultat.getString("grantee"),rezultat.getString("privilege_type"),table_name);
                            }
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                
                }
            }
            
           

            
           


            // punjenje tabela

            for (String table_name : table_names){
                ArrayList<Task> tasks = new ArrayList<Task>();
                ArrayList<Log> logs =  new ArrayList<Log>();
                db.getStatement();
                if (!table_name.equals("logs")){
                    tasks = db.getTasks(table_name);
                    for (Task task : tasks){
                        mydb.createTask(table_name,task);
                    }
                }else{
                    logs = db.getLogs();
                    for(Log log : logs){
                        mydb.createLog(table_name,log);
                    }
                }
            }

            // triggeri

            Set set=db.getAllTriggers().entrySet();
            Iterator itr=set.iterator();  
            while(itr.hasNext()){  
                Map.Entry entry=(Map.Entry)itr.next();  
                String triggerBody= db.getTriggerBody((String)entry.getKey());
                System.out.println(triggerBody);  
            }  

            db.getTriggerProcedure();

          // migracija mongo tabela

           String sql = "CREATE TABLE images( id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,userId BIGINT(19) NOT NULL, file LONGBLOB NOT NULL);";
           mydb.executeSql(sql);
           List<Image> images = imageMongoService.getAllImages();
           if(!images.isEmpty()){
                for (Image image : images){
                    mydb.createImage(image.getUserId(),image.getFile());
                }
           }
        
        }catch(Exception e){
            mydb.rollback();
        }
        mydb.commit();

        
    }

}
