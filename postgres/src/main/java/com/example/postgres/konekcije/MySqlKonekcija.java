package com.example.postgres.konekcije;

import java.sql.*;
import java.util.Date;
import com.example.postgres.model.Task;
import com.example.postgres.model.Log;
import java.util.List;
import java.util.ArrayList;
import javax.json.*;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.node.*;
import org.bson.types.Binary;

public class MySqlKonekcija{

	static String korisnik = "root";
	static String sifra = "bp123"; 
	static String driver = "org.gjt.mm.mysql.Driver"; 
	static String url = "jdbc:mysql://localhost:3306/bptask"; 

	static Connection konekcija = null; 
	private Connection connection; 
	static Statement iskaz = null;
	static PreparedStatement pripremljeniIskaz = null;
	static ResultSet rezultat = null; 
	static DatabaseMetaData metaPodaci = null; 

	public void loadDriver() {
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	public void connect() {
		try {
			konekcija = DriverManager.getConnection(url, korisnik, sifra);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void diskonekt() {
		try {
			konekcija.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void commit() {
		try {
			konekcija.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void rollback(){
		try {
			konekcija.rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void setAutoCommit(boolean autoCommit) {
		try {
			konekcija.setAutoCommit(autoCommit);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public Statement getStatement() {
		try {
			iskaz = konekcija.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return iskaz;
	}

	public void executeSql(String sql){
		try {
			getStatement();
			iskaz.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	public void createNewDatabaseUser(String username){
		String sql = "CREATE USER '"+username+"'@'localhost' IDENTIFIED BY '"+username+"'";
		try {
			getStatement();
			iskaz.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public void addPrivileges(String username, String privilege, String table){
		
		String sql = "GRANT " + privilege+ " ON "+ table+ " TO '"+ username+"'@'localhost';";
		try {
			getStatement();
			System.out.println(sql);
			iskaz.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public void createTask(String table_name, Task task){
		String sql ="INSERT INTO "+ table_name + "(task, comment) VALUES(?, ?);";
		try {
			pripremljeniIskaz = konekcija.prepareStatement(sql);
			pripremljeniIskaz.clearParameters();
			pripremljeniIskaz.setString(1, task.getTask());
			pripremljeniIskaz.setString(2, task.getComment());
			pripremljeniIskaz.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void createLog(String table_name, Log log){
		String sql ="INSERT INTO logs(user_name,action_type,time_stamp) VALUES(?, ?, ?)";
		try {
			pripremljeniIskaz = konekcija.prepareStatement(sql);
			pripremljeniIskaz.clearParameters();
			pripremljeniIskaz.setString(1, log.getUsername());
			pripremljeniIskaz.setString(2, log.getActionType());
			pripremljeniIskaz.setTimestamp(3, new Timestamp((log.getTimestamp()).getTime()));
			pripremljeniIskaz.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void createImage(Long userId, Binary file){
		String sql ="INSERT INTO images(userId,file) VALUES(?, ?)";
		try {
			pripremljeniIskaz = konekcija.prepareStatement(sql);
			pripremljeniIskaz.clearParameters();
			pripremljeniIskaz.setLong(1, userId);
			pripremljeniIskaz.setBytes(2, file.getData());
			pripremljeniIskaz.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public ArrayList<String> getDatabaseUsers(){
		ResultSet rezultat = null;
		ArrayList<String> usernames = new ArrayList<String>();
		String sql = "Select User from mysql.user;";
		try {
			getStatement();
			rezultat = iskaz.executeQuery(sql);
			while(rezultat.next()){
	        	usernames.add(rezultat.getString(1));
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 	usernames;

	}

}