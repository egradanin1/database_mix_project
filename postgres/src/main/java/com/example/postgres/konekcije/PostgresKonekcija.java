package com.example.postgres.konekcije;

import java.sql.*;
import java.util.Date;
import com.example.postgres.model.Task;
import com.example.postgres.model.Log;
import java.util.*;
import javax.json.*;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.node.*;

public class PostgresKonekcija{

	static String korisnik = "postgres";
	static String sifra = "bp123"; 
	static String driver = "org.postgresql.Driver"; 
	static String url = "jdbc:postgresql://localhost:5432/bptask"; 

	static Connection konekcija = null; 
	private Connection connection; 
	static Statement iskaz = null;
	static PreparedStatement pripremljeniIskaz = null;
	static ResultSet rezultat = null; 
	static DatabaseMetaData metaPodaci = null; 

	public void loadDriver() {
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	public void connect() {
		try {
			konekcija = DriverManager.getConnection(url, korisnik, sifra);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void diskonekt() {
		try {
			konekcija.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public Statement getStatement() {
		try {
			iskaz = konekcija.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return iskaz;
	}

	// insert
	public void pripremiInsertToEnisTable() {
		String sql ="INSERT INTO enis_table(task, comment) VALUES(?, ?);INSERT INTO logs(user_name,action_type,time_stamp) VALUES(?, ?, ?)";
		try {
			pripremljeniIskaz = konekcija.prepareStatement(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void insertToEnisTable() {
		try {
			pripremljeniIskaz.clearParameters();
			pripremljeniIskaz.setString(1, "enis_task");
			pripremljeniIskaz.setString(2, "enis_comment");
			pripremljeniIskaz.setString(3, korisnik);
			pripremljeniIskaz.setString(4, "insert on enis_table");
			pripremljeniIskaz.setTimestamp(5, new Timestamp((new Date()).getTime()));
			pripremljeniIskaz.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	public void pripremiInsertToHamzaTable() {
		String sql ="INSERT INTO hamza_table(task, comment) VALUES(?, ?);INSERT INTO logs(user_name,action_type,time_stamp) VALUES(?, ?, ?)";
		try {
			pripremljeniIskaz = konekcija.prepareStatement(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void insertToHamzaTable() {
		try {
			pripremljeniIskaz.clearParameters();
			pripremljeniIskaz.setString(1, "hamza_task");
			pripremljeniIskaz.setString(2, "hamza_comment");
			pripremljeniIskaz.setString(3, korisnik);
			pripremljeniIskaz.setString(4, "insert on hamza_table");
			pripremljeniIskaz.setTimestamp(5, new Timestamp((new Date()).getTime()));
			pripremljeniIskaz.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	public void pripremiInsertToEhvanTable() {
		String sql ="INSERT INTO ehvan_table(task, comment) VALUES(?, ?);INSERT INTO logs(user_name,action_type,time_stamp) VALUES(?, ?, ?)";
		try {
			pripremljeniIskaz = konekcija.prepareStatement(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void insertToEhvanTable() {
		try {
			pripremljeniIskaz.clearParameters();
			pripremljeniIskaz.setString(1, "ehvan_task");
			pripremljeniIskaz.setString(2, "ehvan_comment");
			pripremljeniIskaz.setString(3, korisnik);
			pripremljeniIskaz.setString(4, "insert on ehvan_table");
			pripremljeniIskaz.setTimestamp(5, new Timestamp((new Date()).getTime()));
			pripremljeniIskaz.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}


	// update
	public void pripremiUpdateEnisTable() {
		String sql ="UPDATE enis_table set task = ? where id = ?;INSERT INTO logs(user_name,action_type,time_stamp) VALUES(?, ?, ?)";
		try {
			pripremljeniIskaz = konekcija.prepareStatement(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void updateEnisTable() {
		try {
			pripremljeniIskaz.clearParameters();
			pripremljeniIskaz.setString(1, "novi_enis_task");
			pripremljeniIskaz.setInt(2, 1);
			pripremljeniIskaz.setString(3, korisnik);
			pripremljeniIskaz.setString(4, "update on enis_table");
			pripremljeniIskaz.setTimestamp(5, new Timestamp((new Date()).getTime()));
			pripremljeniIskaz.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	public void pripremiUpdateHamzaTable() {
		String sql ="UPDATE hamza_table set task = ? where id = ?;INSERT INTO logs(user_name,action_type,time_stamp) VALUES(?, ?, ?)";
		try {
			pripremljeniIskaz = konekcija.prepareStatement(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void updateHamzaTable() {
		try {
			pripremljeniIskaz.clearParameters();
			pripremljeniIskaz.setString(1, "novi_hamza_task");
			pripremljeniIskaz.setInt(2, 1);
			pripremljeniIskaz.setString(3, korisnik);
			pripremljeniIskaz.setString(4, "update on hamza_table");
			pripremljeniIskaz.setTimestamp(5, new Timestamp((new Date()).getTime()));
			pripremljeniIskaz.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	public void pripremiUpdateEhvanTable() {
		String sql ="UPDATE ehvan_table set task = ? where id = ?;INSERT INTO logs(user_name,action_type,time_stamp) VALUES(?, ?, ?)";
		try {
			pripremljeniIskaz = konekcija.prepareStatement(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void updateEhvanTable() {
		try {
			pripremljeniIskaz.clearParameters();
			pripremljeniIskaz.setString(1, "novi_ehvan_task");
			pripremljeniIskaz.setInt(2, 1);
			pripremljeniIskaz.setString(3, korisnik);
			pripremljeniIskaz.setString(4, "update on ehvan_table");
			pripremljeniIskaz.setTimestamp(5, new Timestamp((new Date()).getTime()));
			pripremljeniIskaz.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	// delete

	public void pripremiDeleteEnisTable() {
		String sql ="DELETE FROM enis_table WHERE id = ?;INSERT INTO logs(user_name,action_type,time_stamp) VALUES(?, ?, ?)";
		try {
			pripremljeniIskaz = konekcija.prepareStatement(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void deleteEnisTable() {
		try {
			pripremljeniIskaz.clearParameters();
			pripremljeniIskaz.setInt(1, 2);
			pripremljeniIskaz.setString(2, korisnik);
			pripremljeniIskaz.setString(3, "delete on enis_table");
			pripremljeniIskaz.setTimestamp(4, new Timestamp((new Date()).getTime()));
			pripremljeniIskaz.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	public void pripremiDeleteHamzaTable() {
		String sql ="DELETE FROM hamza_table WHERE id = ?;INSERT INTO logs(user_name,action_type,time_stamp) VALUES(?, ?, ?)";
		try {
			pripremljeniIskaz = konekcija.prepareStatement(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void deleteHamzaTable() {
		try {
			pripremljeniIskaz.clearParameters();
			pripremljeniIskaz.setInt(1, 2);
			pripremljeniIskaz.setString(2, korisnik);
			pripremljeniIskaz.setString(3, "delete on hamza_table");
			pripremljeniIskaz.setTimestamp(4, new Timestamp((new Date()).getTime()));
			pripremljeniIskaz.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	public void pripremiDeleteEhvanTable() {
		String sql ="DELETE FROM ehvan_table WHERE id = ?;INSERT INTO logs(user_name,action_type,time_stamp) VALUES(?, ?, ?)";
		try {
			pripremljeniIskaz = konekcija.prepareStatement(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void deleteEhvanTable() {
		try {
			pripremljeniIskaz.clearParameters();
			pripremljeniIskaz.setInt(1, 2);
			pripremljeniIskaz.setString(2, korisnik);
			pripremljeniIskaz.setString(3, "delete on ehvan_table");
			pripremljeniIskaz.setTimestamp(4, new Timestamp((new Date()).getTime()));
			pripremljeniIskaz.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public ArrayList<Task> getTasks(String tabela) {
		ArrayList<Task> tasks = new ArrayList<Task>();
		String sql = "SELECT id,task,comment FROM "+ tabela;
		try {
			rezultat = iskaz.executeQuery(sql);
			while(rezultat.next()){
	        	Integer id = rezultat.getInt("id");
	        	String task = rezultat.getString("task");
	        	String comment = rezultat.getString("comment");
	        	tasks.add(new Task(id,task,comment));
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tasks;
	}

	public ArrayList<Log> getLogs() {
		ArrayList<Log> logs = new ArrayList<Log>();
		String sql = "SELECT id,user_name,action_type,time_stamp FROM logs";
		try {
			rezultat = iskaz.executeQuery(sql);
			while(rezultat.next()){
	        	Integer id = rezultat.getInt("id");
	        	String user_name = rezultat.getString("user_name");
	        	String action_type = rezultat.getString("action_type");
	        	Timestamp time_stamp = rezultat.getTimestamp("time_stamp");
	        	logs.add(new Log(id,user_name,action_type,time_stamp));
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return logs;
	}

	public JsonNode getMetaData(){
		ObjectMapper  objectMapper = new ObjectMapper();
	    ObjectNode metaData = objectMapper.createObjectNode();
		try{
			metaPodaci = konekcija.getMetaData();
			/*Skup osobina*/

	        metaData.put("Database Product Name",metaPodaci.getDatabaseProductName());
	        metaData.put("Driver Name", metaPodaci.getDriverName());
	        metaData.put("Extra Name Characters",metaPodaci.getExtraNameCharacters());
	        metaData.put("Max Column Name Length",  metaPodaci.getMaxColumnNameLength());
	        metaData.put("Max Catalog Name Length", metaPodaci.getMaxCatalogNameLength());
	        metaData.put("Max Columns In Group By",  metaPodaci.getMaxColumnsInGroupBy());
	        metaData.put("Max Columns In Index",metaPodaci.getMaxColumnsInIndex());
	        metaData.put("Max Columns In Order By", metaPodaci.getMaxColumnsInOrderBy());
	        metaData.put("Max Columns In Select",metaPodaci.getMaxColumnsInSelect());
	        metaData.put("Max Columns In Table",metaPodaci.getMaxColumnsInTable());
			metaData.put("Max Cursor Name Length",metaPodaci.getMaxCursorNameLength());
			metaData.put("Max Index Length",metaPodaci.getMaxIndexLength());
			metaData.put("Max Procedure Name Length",metaPodaci.getMaxProcedureNameLength());
			metaData.put("Max Row Size",metaPodaci.getMaxRowSize());
			metaData.put("Max Schema Name Length",metaPodaci.getMaxSchemaNameLength());
			metaData.put("Max Statement Length",metaPodaci.getMaxStatementLength());
			metaData.put("Max Table Name Length",metaPodaci.getMaxTableNameLength());
			metaData.put("Max Tables In Select", metaPodaci.getMaxTablesInSelect());
			metaData.put("SQL Keywords", metaPodaci.getSQLKeywords());
			metaData.put("SQL State Type",metaPodaci.getSQLStateType());
			metaData.put("Numeric Functions",metaPodaci.getNumericFunctions());
			metaData.put("String Functions", metaPodaci.getStringFunctions());
			metaData.put("System Functions",metaPodaci.getSystemFunctions());
			metaData.put("Time Date Functions",metaPodaci.getTimeDateFunctions());
			metaData.put("URL", metaPodaci.getURL());
			metaData.put("User Name",metaPodaci.getUserName());
			metaData.put("Supports Like Escape Clause",metaPodaci.supportsLikeEscapeClause());
			metaData.put("Sql State SQL99", metaPodaci.sqlStateSQL99);
			metaData.put("Supports Stored Procedures", metaPodaci.supportsStoredProcedures());


			/* Spisak kataloga(baza) */
			
			List<String> spisakKataloga = new ArrayList<String>();
			rezultat = metaPodaci.getCatalogs();
			while (rezultat.next()) {
				spisakKataloga.add(rezultat.getString(1));
			}
			ArrayNode array =  objectMapper.valueToTree(spisakKataloga);
			metaData.putArray("Katalozi").addAll(array);
			String nazivKataloga = spisakKataloga.get(0);


			/* Opis seme podataka */
			List<ObjectNode> tabeleInfo = new ArrayList<ObjectNode>();
			String tipovi[] = { "TABLE", "TRIGGER"};
			rezultat =  metaPodaci.getTables(null, "%", "%", tipovi);
			List<String> tablesNames = new ArrayList<String>();

			while(rezultat.next()){
				ObjectNode sema = objectMapper.createObjectNode();
				sema.put("Ime Baze", nazivKataloga);
				sema.put("Ime Tabela", rezultat.getString(3));
				tablesNames.add(rezultat.getString(3));
				sema.put("Tip Tabela", rezultat.getString(4));
				tabeleInfo.add(sema);
			}
			array =  objectMapper.valueToTree(tabeleInfo);
			metaData.putArray("Info o tabelama").addAll(array);

			/* Kolone */
			ObjectNode tabele = objectMapper.createObjectNode();

			for (String tableName : tablesNames) {
				
				List<ObjectNode> tabelaKoloneInfo = new ArrayList<ObjectNode>();
				rezultat = metaPodaci.getColumns(null, "%", tableName, "%");

				while(rezultat.next()){
					ObjectNode kolona = objectMapper.createObjectNode();
					kolona.put("Ime Kolone", rezultat.getString("COLUMN_NAME"));
					kolona.put("Tip Kolone", rezultat.getString("TYPE_NAME"));
					kolona.put("Velicina Kolone", rezultat.getInt("COLUMN_SIZE"));
					kolona.put("Cifre",rezultat.getInt("DECIMAL_DIGITS"));

					int nullable = rezultat.getInt("NULLABLE");
					boolean isNull = (nullable == 1);
					String nul;
					if (isNull) kolona.put("Is null","NULL");
					else kolona.put("Is null","NOT NULL");
					tabelaKoloneInfo.add(kolona);
				}
				tabele.putArray(tableName).addAll(tabelaKoloneInfo);
			}

			metaData.put("Kolone",tabele);

			/* Primarni kljucevi */


			List<ObjectNode> tabeleKljucevi = new ArrayList<ObjectNode>();

			for (String tableName : tablesNames) {
				ObjectNode tabela = objectMapper.createObjectNode();
				List<ObjectNode> tabelaKljuceviInfo = new ArrayList<ObjectNode>();
			    rezultat = metaPodaci.getPrimaryKeys(null, null,tableName);

				while(rezultat.next()){
					ObjectNode kljuc = objectMapper.createObjectNode();
					kljuc.put("Redni broj kolone", rezultat.getInt("KEY_SEQ"));
					kljuc.put("Naziv kolone", rezultat.getString("COLUMN_NAME"));

					tabelaKljuceviInfo.add(kljuc);
				}
				tabela.putArray(tableName).addAll(tabelaKljuceviInfo);
				tabeleKljucevi.add(tabela);
			}

			array = objectMapper.valueToTree(tabeleKljucevi);
			metaData.putArray("Primarni kljucevi").addAll(array);




		}catch(Exception e){
			e.printStackTrace();
		}
		return (JsonNode) metaData;	
	}

	public ResultSet getUsersPrivileges(String tableName){
		ResultSet rezultat = null;
		String sql = "SELECT grantee, privilege_type FROM information_schema.role_table_grants WHERE table_name='"+ tableName+ "'";
		try {
			getStatement();
			rezultat = iskaz.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rezultat;
		
	}

	public ArrayList<String> getDatabaseUsers(){
		ResultSet rezultat = null;
		ArrayList<String> usernames = new ArrayList<String>();
		String sql = "SELECT usename FROM pg_user;";
		try {
			getStatement();
			rezultat = iskaz.executeQuery(sql);
			while(rezultat.next()){
	        	usernames.add(rezultat.getString(1));
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 	usernames;

	}

	public Map getAllTriggers(){
		ResultSet rezultat = null;
		Map triggers =new HashMap();  
		String sql = "SELECT trigger_name,event_object_table FROM information_schema.triggers group by trigger_name,event_object_table;";
		try {
			getStatement();
			rezultat = iskaz.executeQuery(sql);
			while(rezultat.next()){
				triggers.put(rezultat.getString(1),rezultat.getString(2));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return triggers;

	}

	public String getTriggerBody(String triggerName){
		String triggerBody = "";
		String sql = "select pg_get_triggerdef(oid) from pg_trigger where tgname = '" + triggerName + "';";
		try {
			getStatement();
			rezultat = iskaz.executeQuery(sql);
			while(rezultat.next()){
				System.out.println(rezultat.getString(1));
	        	triggerBody += rezultat.getString(1);
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 	triggerBody;
	}

	public String getTriggerProcedure(){
		String triggerProcedure = "";
		String sql = "select pg_get_functiondef('public.activate_hamza_trigger()'::regprocedure);";
		try {
			getStatement();
			rezultat = iskaz.executeQuery(sql);
			while(rezultat.next()){
				System.out.println(rezultat.getString(1));
	        	triggerProcedure += rezultat.getString(1);
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 	triggerProcedure;
	}


}