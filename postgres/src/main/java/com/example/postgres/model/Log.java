package com.example.postgres.model;


import java.util.Set;
import java.time.LocalDateTime;

import java.util.List;
import java.util.ArrayList;
import java.sql.Timestamp;



public class Log{

    private Integer id;
    private String user_name;
    private String action_type;
    private Timestamp time_stamp;


    public Log() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return user_name;
    }

    public void setUsername(String user_name) {
        this.user_name = user_name;
    }

    public String getActionType() {
        return action_type;
    }

    public void setActionType(String action_type) {
        this.action_type = action_type;
    }    

    public Timestamp getTimestamp(){
    	return time_stamp;
    }

    public void setTimestamp(Timestamp time_stamp){
    	this.time_stamp = time_stamp;
    }

    public Log(Integer id, String user_name, String action_type, Timestamp time_stamp){
        this.id = id;
        this.user_name = user_name;
        this.action_type = action_type;
        this.time_stamp = time_stamp;
    }
}
