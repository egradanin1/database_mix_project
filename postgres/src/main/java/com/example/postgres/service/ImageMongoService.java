package com.example.postgres.service;

import com.example.postgres.model.Image;
import java.util.List;
import java.util.Collection;


/** Represents an image mongo service interface.
 * @author Ehvan Gradanin
*/

public interface ImageMongoService {
    /** 
     * Saves image to database.
     *
     * @param  image POJO Image 
     */
    void save(Image image);
    /**
     * Returns list of all images in datatabase.
     * @return list of images
     */
    List<Image> getAllImages();
    /**
     * Returns Image that belongs to user with the userId.
     * @param  userId id of user that we are searching image for
     * @return image found by userId
     */
    Image findByUserId(Long userId);
}
