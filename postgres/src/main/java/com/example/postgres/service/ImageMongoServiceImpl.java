package com.example.postgres.service;

import com.example.postgres.model.Image;
import com.example.postgres.mongorepository.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.HashSet;
import java.util.Arrays;
import java.util.List;

/** Represents an image mongo service interface implementation.
 * @author Ehvan Gradanin
*/

@Service
public class ImageMongoServiceImpl implements ImageMongoService {
    @Autowired
    /** Represents the ImageRepository.
    */
    private ImageRepository imageRepository;


    @Override
    /**
     * Calls imageRepository to save image to mongo database.
     *
     * @param  image POJO Image 
     */
    public void save(Image image) {
        imageRepository.save(image);
    }

    @Override
    /**
     * Returns list of all images in datatabase using imageRepository method findAll.
     * @return list of images
     */
    public List<Image> getAllImages() {
        return imageRepository.findAll();
    }

    @Override
    /**
     * Returns Image that belongs to user with the userId.
     * @param  userId id of user that we are searching image for
     * @return image found by userId
     */
    public Image findByUserId(Long userId){
        return imageRepository.findByUserId(userId);
    }
}
