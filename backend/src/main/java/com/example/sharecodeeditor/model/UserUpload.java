package com.example.sharecodeeditor.model;

import java.util.UUID;
import javax.persistence.*;
import java.util.Set;
import java.time.LocalDateTime;
import java.util.List;
import java.util.ArrayList;
import org.bson.types.Binary;
import com.example.sharecodeeditor.model.UserData;

@Entity
public class UserUpload{

    @Id
    private Long id;

    private String naziv;
    private String type;
    private String image;

    @Lob
    private Binary data;

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = UserData.class)
    @JoinColumn(name = "user_id", nullable = false)
    private UserData user;


    public UserUpload() {}

    public UserUpload(Long id,String naziv,String type,String image,Binary data,UserData user) {
        this.id = id;
        this.naziv = naziv;
        this.type = type;
        this.image = image;
        this.data = data;
        this.user = user;
    }
    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Binary getData(){
        return data;
    }

    public void setData(Binary data){
        this.data = data;
    }

    public String getNaziv()
    {
        return this.naziv;
    }

    public void setNaziv(String naziv)
    {
        this.naziv = naziv;
    }

    public String getImage()
    {
        return this.image;
    }

    public void setImage(String image)
    {
        this.image = image;
    }

    public String getType()
    {
        return this.type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public UserData getUser()
    {
        return this.user;
    }

    public void setUser(UserData user)
    {
        this.user = user;
    }


}
