package com.example.sharecodeeditor.model;

import java.util.UUID;
import javax.persistence.*;
import java.util.Set;
import java.time.LocalDateTime;
//import org.springframework.data.annotation.Id;
import java.util.List;
import java.util.ArrayList;
import com.example.sharecodeeditor.model.UserData;

@Entity
public class Settings{

	@Id
    private Long id;

    private String backgroundColor;
    private String codeColor;
    private String font;

    public Settings() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private UserData user;



    public String getbackgroundColor() {
        return backgroundColor;
    }

    public void setbackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getcodeColor() {
        return codeColor;
    }

    public void setcodeColor(String codeColor) {
        this.codeColor = codeColor;
    }

    public String getfont() {
        return font;
    }

    public void setfont(String font) {
        this.font = font;
    }

}
