package com.example.sharecodeeditor.model;

import java.util.UUID;
import javax.persistence.*;
import java.util.Set;
import java.time.LocalDateTime;
//import org.springframework.data.annotation.Id;
import java.util.List;
import java.util.ArrayList;
import com.example.sharecodeeditor.model.UserData;


@Entity
public class Friends{

    @Id
    private Long id;

    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user1", nullable = false)
    private UserData user1;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user2", nullable = false)
    private UserData user2;



    public Friends() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserData getUser1() {
        return user1;
    }

    public void setUser1(UserData user1) {
        this.user1 = user1;
    }

    public UserData getUser2() {
        return user2;
    }

    public void setUser2(UserData user2) {
        this.user2 = user2;
    }

    

}
