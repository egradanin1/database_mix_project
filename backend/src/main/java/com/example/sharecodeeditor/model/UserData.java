package com.example.sharecodeeditor.model;

import java.util.UUID;
import javax.persistence.*;
import java.util.Set;
import java.time.LocalDateTime;
import java.util.List;
import java.util.ArrayList;

/** Represents an user.
 * @author Ehvan Gradanin
*/

@Entity
@Table(indexes = {
        @Index(columnList = "username", name = "username_idx"),
})
public class UserData{

    @Id
    private Long id;
    /** Represents the users’s firstname.
    */
    private String firstName;
    /** Represents the users’s lastname.
    */
    private String lastName;
    /** Represents the users’s username.
    */
    private String username;
    /** Represents the users’s password.
    */
    private String password;
    /** Represents the users’s email.
    */
    private String email;

    public UserData() {
    }
    /** Creates an user with the specified id,firstname,lastname,username,password,email.
    * @param id The users’s id.
    * @param firstName The users’s first name.
    * @param lastName The users’s last name.
    * @param username The users’s username.
    * @param password The users’s password.
    * @param email The users’s email.
    */
    public UserData(Long id,String firstName,String  lastName,String username,String  password,String  email) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.email = email;
    }

    /** Gets the user’s id.
    * @return A long representing the user’s id.
    */
    public Long getId() {
        return id;
    }
    /** Sets the user’s id.
    * @param id A Long containing the user’s id.
    */
    public void setId(Long id) {
        this.id = id;
    }
    /** Gets the user’s first name.
    * @return A String representing the user’s first name.
    */
    public String getFirstName() {
        return firstName;
    }
    /** Sets the user’s first name.
    * @param firstName A String containing the user’s first name.
    */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    /** Gets the user’s last name.
    * @return A String representing the user’s last name.
    */
    public String getLastName() {
        return lastName;
    }
     /** Sets the user’s last name.
    * @param lastName A String containing the user’s last name.
    */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    /** Gets the user’s username.
    * @return A String representing the user’s username.
    */
    public String getUsername() {
        return username;
    }
    /** Sets the user’s username.
    * @param username A String containing the user’s username.
    */
    public void setUsername(String username) {
        this.username = username;
    }
    /** Gets the user’s password.
    * @return A String representing the user’s username.
    */
    public String getPassword() {
        return password;
    }
    /** Sets the user’s password.
    * @param password A String containing the user’s password.
    */
    public void setPassword(String password) {
        this.password = password;
    }
    /** Gets the user’s email.
    * @return A String representing the user’s email.
    */
    public String getEmail() {
        return email;
    }
     /** Sets the user’s email.
    * @param email A String containing the user’s email.
    */
    public void setEmail(String email) {
        this.email = email;
    }
}
