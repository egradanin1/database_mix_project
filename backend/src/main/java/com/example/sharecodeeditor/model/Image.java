package com.example.sharecodeeditor.model;

import java.util.UUID;
import javax.persistence.*;
import java.util.Set;
import java.time.LocalDateTime;
import java.util.List;
import java.util.ArrayList;
import org.bson.types.Binary;


public class Image{

    @Id
    private UUID id;

    private Long userId;
    private Binary file;

    public Image() {}

    public Image(UUID id,Long userId,Binary file) {
        this.id = id;
        this.userId = userId;
        this.file = file;
    }
    public UUID getId()
    {
        return this.id;
    }

    public void setId(UUID id)
    {
        this.id = id;
    }

    public Long getUserId(){
        return userId;
    }

    public void setUserId(Long userId){
        this.userId = userId;
    }

    public Binary getFile(){
        return file;
    }

    public void setFile(Binary file){
        this.file = file;
    }

}
