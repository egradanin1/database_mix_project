package com.example.sharecodeeditor.model;


import java.util.Set;
import java.time.LocalDateTime;

import java.util.List;
import java.util.ArrayList;



public class Task{

    private Integer id;
    private String task;
    private String comment;

    public Task() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }    

    public Task(Integer id, String task, String comment){
        this.id = id;
        this.task = task;
        this.comment = comment;
    }
}
