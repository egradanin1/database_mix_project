package com.example.sharecodeeditor.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.sharecodeeditor.model.UserData;

/** Represents an userdata crud repository.
 * @author Ehvan Gradanin
*/

@Repository
public interface UserDataRepository extends JpaRepository<UserData, Long> {
	/**
	 * Returns UserData with id.
	 * @param  id id of user that we are searching for
	 * @return UserData object 
	 */
	UserData findById(long id);
	/**
	 * Returns list of all userdatas in datatabase.
	 * @return list of userdata
	 */
	List<UserData> findAll();
	/**
	 * Returns UserData object after saving it to mongo database.
	 *
	 * @param  user POJO UserData 
	 * @return saved UserData
	 */
	UserData save(UserData user);
	/**
	 * Returns UserData with username.
	 * @param  username username of user that we are searching for
	 * @return UserData object 
	 */
	UserData findByUsername(String username);
	void delete(UserData userData);
}
