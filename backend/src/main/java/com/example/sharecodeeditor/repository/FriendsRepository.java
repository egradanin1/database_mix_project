package com.example.sharecodeeditor.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.sharecodeeditor.model.Friends;

@Repository
public interface FriendsRepository extends CrudRepository<Friends, Long> {

	Friends findFriendsById(long id);
}
