package com.example.sharecodeeditor.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.sharecodeeditor.model.Settings;

@Repository
public interface SettingsRepository extends CrudRepository<Settings, Long> {

	Settings findSettingsById(long id);
}
