package com.example.sharecodeeditor.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.sharecodeeditor.model.UserUpload;

/** Represents an attachment crud repository.
 * @author Ehvan Gradanin
*/

@Repository
public interface UserUploadRepository extends JpaRepository<UserUpload, Long> {
	/**
	 * Returns UserUpload with id.
	 * @param  id id of UserUpload that we are searching for
	 * @return userUploadsobject 
	 */
	UserUpload findById(long id);
	/**
	 * Returns list of all UserUploads in datatabase.
	 * @return list of UserUploads
	 */
	List<UserUpload> findAll();
	/**
	 * Returns UserUpload object after saving it to oracle database.
	 *
	 * @param  userUpload POJO UserUpload 
	 * @return saved UserUpload
	 */
	UserUpload save(UserUpload userUpload);

	/**
	 * Returns UserUpload with naziv.
	 * @param  naziv naziv of UserUpload that we are searching for
	 * @return userUploads object 
	 */
	UserUpload findByNaziv(String naziv);
	void delete(UserUpload userUpload);


}
