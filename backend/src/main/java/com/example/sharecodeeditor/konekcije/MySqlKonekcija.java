package com.example.sharecodeeditor.konekcije;

import java.sql.*;
import java.util.Date;
import com.example.sharecodeeditor.model.Task;
import com.example.sharecodeeditor.model.Log;
import com.example.sharecodeeditor.model.UserData;
import com.example.sharecodeeditor.model.UserUpload;
import java.util.*;
import javax.json.*;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.node.*;
import org.bson.types.Binary;
import org.bson.types.*;
import org.bson.BsonBinarySubType;

public class MySqlKonekcija{

	static String korisnik = "root";
	static String sifra = "bp123"; 
	static String driver = "org.gjt.mm.mysql.Driver"; 
	static String url = "jdbc:mysql://localhost:3306/bptask"; 

	static Connection konekcija = null; 
	private Connection connection; 
	static Statement iskaz = null;
	static PreparedStatement pripremljeniIskaz = null;
	static ResultSet rezultat = null; 
	static ResultSet userRezultat = null; 
	static DatabaseMetaData metaPodaci = null; 

	public void loadDriver() {
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	public void connect() {
		try {
			konekcija = DriverManager.getConnection(url, korisnik, sifra);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void diskonekt() {
		try {
			konekcija.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void commit() {
		try {
			konekcija.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void rollback(){
		try {
			konekcija.rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void setAutoCommit(boolean autoCommit) {
		try {
			konekcija.setAutoCommit(autoCommit);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public Statement getStatement() {
		try {
			iskaz = konekcija.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return iskaz;
	}

	public void executeSql(String sql){
		try {
			getStatement();
			iskaz.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	public void createNewDatabaseUser(String username){
		String sql = "CREATE USER '"+username+"'@'localhost' IDENTIFIED BY '"+username+"'";
		try {
			getStatement();
			iskaz.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public void addPrivileges(String username, String privilege, String table){
		
		String sql = "GRANT " + privilege+ " ON "+ table+ " TO '"+ username+"'@'localhost';";
		try {
			getStatement();
			System.out.println(sql);
			iskaz.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public void createTask(String table_name, Task task){
		String sql ="INSERT INTO "+ table_name + "(task, comment) VALUES(?, ?);";
		try {
			pripremljeniIskaz = konekcija.prepareStatement(sql);
			pripremljeniIskaz.clearParameters();
			pripremljeniIskaz.setString(1, task.getTask());
			pripremljeniIskaz.setString(2, task.getComment());
			pripremljeniIskaz.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void createLog(String table_name, Log log){
		String sql ="INSERT INTO logs(user_name,action_type,time_stamp) VALUES(?, ?, ?);";
		try {
			pripremljeniIskaz = konekcija.prepareStatement(sql);
			pripremljeniIskaz.clearParameters();
			pripremljeniIskaz.setString(1, log.getUsername());
			pripremljeniIskaz.setString(2, log.getActionType());
			pripremljeniIskaz.setTimestamp(3, new Timestamp((log.getTimestamp()).getTime()));
			pripremljeniIskaz.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void updateLog(String username,String action_type,Timestamp time_stamp){
		String sql ="INSERT INTO logs(user_name,action_type,time_stamp) VALUES(?, ?, ?);";
		try {
			pripremljeniIskaz = konekcija.prepareStatement(sql);
			pripremljeniIskaz.clearParameters();
			pripremljeniIskaz.setString(1, username);
			pripremljeniIskaz.setString(2, action_type);
			pripremljeniIskaz.setTimestamp(3,time_stamp);
			pripremljeniIskaz.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void createImage(Long userId, Binary file){
		String sql ="INSERT INTO images(userId,file) VALUES(?, ?)";
		try {
			pripremljeniIskaz = konekcija.prepareStatement(sql);
			pripremljeniIskaz.clearParameters();
			pripremljeniIskaz.setLong(1, userId);
			pripremljeniIskaz.setBytes(2, file.getData());
			pripremljeniIskaz.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}


	public void createUser(Long userId, String firstname,String lastname,String username,String password,String email){
		String sql ="INSERT INTO user_data(id,firstname,lastname,username,password,email) VALUES(?, ?, ?, ?, ?, ?)";
		try {
			pripremljeniIskaz = konekcija.prepareStatement(sql);
			pripremljeniIskaz.clearParameters();
			pripremljeniIskaz.setLong(1, userId);
			pripremljeniIskaz.setString(2,firstname);
			pripremljeniIskaz.setString(3,lastname);
			pripremljeniIskaz.setString(4,username);
			pripremljeniIskaz.setString(5,password);
			pripremljeniIskaz.setString(6,email);
			pripremljeniIskaz.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void createUserUpload(Long id, String naziv,String type,String image,Binary  data,Long userId){
		String sql ="INSERT INTO user_upload(id,naziv,type,image,data,user) VALUES(?, ?, ?, ?, ?, ?)";
		try {
			pripremljeniIskaz = konekcija.prepareStatement(sql);
			pripremljeniIskaz.clearParameters();
			pripremljeniIskaz.setLong(1, id);
			pripremljeniIskaz.setString(2,naziv);
			pripremljeniIskaz.setString(3,type);
			pripremljeniIskaz.setString(4,image);
			pripremljeniIskaz.setBytes(5,data.getData());
			pripremljeniIskaz.setLong(6,userId);
			pripremljeniIskaz.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	public UserData getUserWithUsername(String username){
		UserData user = null;
		String sql = "SELECT id,firstname,lastname,username,password,email FROM user_data WHERE username='" + username +"'";
		try {
			getStatement();
			rezultat = iskaz.executeQuery(sql);
			while(rezultat.next()){
	        	Long id = rezultat.getLong("id");
	        	String firstname = rezultat.getString("firstname");
	        	String lastname = rezultat.getString("lastname");
	        	String username_from_database = rezultat.getString("username");
	        	String password = rezultat.getString("password");
	        	String email = rezultat.getString("email");
	        	user = new UserData(id,firstname,lastname,username_from_database,password,email);
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;

	}

	public UserData getUserWithId(Long id){
		UserData user = null;
		String sql = "SELECT id,firstname,lastname,username,password,email FROM user_data WHERE id='" + id +"'";
		try {
			getStatement();
			userRezultat = iskaz.executeQuery(sql);
			while(userRezultat.next()){
	        	Long id_from_database = userRezultat.getLong("id");
	        	String firstname = userRezultat.getString("firstname");
	        	String lastname = userRezultat.getString("lastname");
	        	String username_from_database = userRezultat.getString("username");
	        	String password = userRezultat.getString("password");
	        	String email = userRezultat.getString("email");
	        	user = new UserData(id_from_database,firstname,lastname,username_from_database,password,email);
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;

	}

	public UserUpload getUserUploadByNaziv(String naziv){
		UserUpload userUpload = null;
		String sql = "SELECT id,naziv,type,image,data,user FROM user_upload WHERE naziv='" + naziv +"'";
		try {
			getStatement();
			rezultat = iskaz.executeQuery(sql);
			while(rezultat.next()){
	        	Long id = rezultat.getLong("id");
	        	String naziv_from_database = rezultat.getString("naziv");
	       		String type = rezultat.getString("type");
	        	String image = rezultat.getString("image");
	        	Binary data = new Binary(BsonBinarySubType.BINARY, rezultat.getBytes("data"));
	        	Long userId = rezultat.getLong("user");
	        	userUpload = new UserUpload(id,naziv_from_database,type,image,data,getUserWithId(userId));
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userUpload;

	}

	public ArrayList<UserData> getAllUsers() {
		ArrayList<UserData> users = new ArrayList<UserData>();
		String sql = "SELECT id,firstname,lastname,username,password,email  FROM user_data";
		try {
			getStatement();
			rezultat = iskaz.executeQuery(sql);
			while(rezultat.next()){
	        	Long id = rezultat.getLong("id");
	        	String firstname = rezultat.getString("firstname");
	        	String lastname = rezultat.getString("lastname");
	        	String username_from_database = rezultat.getString("username");
	        	String password = rezultat.getString("password");
	        	String email = rezultat.getString("email");
	        	users.add(new UserData(id,firstname,lastname,username_from_database,password,email));
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return users;
	}
	public ArrayList<UserUpload> getAllUserUploads() {
		ArrayList<UserUpload> usersUploads = new ArrayList<UserUpload>();
		String sql = "SELECT id,naziv,type,image,data,user FROM user_upload";
		try {
			getStatement();
			rezultat = iskaz.executeQuery(sql);
			while(rezultat.next()){
	        	Long id = rezultat.getLong("id");
	        	String naziv = rezultat.getString("naziv");
	       		String type = rezultat.getString("type");
	        	String image = rezultat.getString("image");
	        	Binary data = new Binary(BsonBinarySubType.BINARY, rezultat.getBytes("data"));
	        	Long userId = rezultat.getLong("user");
	        	UserUpload userUploadToStore = new UserUpload(id,naziv,type,image,data,getUserWithId(userId));
	        	// System.out.println(userUploadToStore.getNaziv()+ " " + userUploadToStore.getUser().getId());
	        	usersUploads.add(userUploadToStore);
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return usersUploads;
	}
	
	

	public ArrayList<String> getDatabaseUsers(){
		ResultSet rezultat = null;
		ArrayList<String> usernames = new ArrayList<String>();
		String sql = "Select User from mysql.user;";
		try {
			getStatement();
			rezultat = iskaz.executeQuery(sql);
			while(rezultat.next()){
	        	usernames.add(rezultat.getString(1));
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 	usernames;

	}

	// Postgres kopiranje


	// insert

	
	public void pripremiInsertToEnisTable() {
		String sql ="INSERT INTO enis_table(task, comment) VALUES(?, ?);";
		try {
			pripremljeniIskaz = konekcija.prepareStatement(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void insertToEnisTable() {
		try {
			pripremljeniIskaz.clearParameters();
			pripremljeniIskaz.setString(1, "enis_task");
			pripremljeniIskaz.setString(2, "enis_comment");
			pripremljeniIskaz.executeUpdate();
			updateLog(korisnik,"insert on enis_table",new Timestamp((new Date()).getTime()));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	public void pripremiInsertToHamzaTable() {
		String sql ="INSERT INTO hamza_table(task, comment) VALUES(?, ?);";
		try {
			pripremljeniIskaz = konekcija.prepareStatement(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void insertToHamzaTable() {
		try {
			pripremljeniIskaz.clearParameters();
			pripremljeniIskaz.setString(1, "hamza_task");
			pripremljeniIskaz.setString(2, "hamza_comment");
			pripremljeniIskaz.executeUpdate();
			updateLog(korisnik,"insert on hamza_table",new Timestamp((new Date()).getTime()));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	public void pripremiInsertToEhvanTable() {
		String sql ="INSERT INTO ehvan_table(task, comment) VALUES(?, ?);";
		try {
			pripremljeniIskaz = konekcija.prepareStatement(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void insertToEhvanTable() {
		try {
			pripremljeniIskaz.clearParameters();
			pripremljeniIskaz.setString(1, "ehvan_task");
			pripremljeniIskaz.setString(2, "ehvan_comment");
			pripremljeniIskaz.executeUpdate();
			updateLog(korisnik,"insert on ehvan_table",new Timestamp((new Date()).getTime()));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}


	// update
	public void pripremiUpdateEnisTable() {
		String sql ="UPDATE enis_table set task = ? where id = ?;";
		try {
			pripremljeniIskaz = konekcija.prepareStatement(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void updateEnisTable() {
		try {
			pripremljeniIskaz.clearParameters();
			pripremljeniIskaz.setString(1, "novi_enis_task");
			pripremljeniIskaz.setInt(2, 1);
			pripremljeniIskaz.executeUpdate();
			updateLog(korisnik,"update on enis_table",new Timestamp((new Date()).getTime()));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	public void pripremiUpdateHamzaTable() {
		String sql ="UPDATE hamza_table set task = ? where id = ?;";
		try {
			pripremljeniIskaz = konekcija.prepareStatement(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void updateHamzaTable() {
		try {
			pripremljeniIskaz.clearParameters();
			pripremljeniIskaz.setString(1, "novi_hamza_task");
			pripremljeniIskaz.setInt(2, 1);
			pripremljeniIskaz.executeUpdate();
			updateLog(korisnik,"update on hamza_table",new Timestamp((new Date()).getTime()));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	public void pripremiUpdateEhvanTable() {
		String sql ="UPDATE ehvan_table set task = ? where id = ?;";
		try {
			pripremljeniIskaz = konekcija.prepareStatement(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void updateEhvanTable() {
		try {
			pripremljeniIskaz.clearParameters();
			pripremljeniIskaz.setString(1, "novi_ehvan_task");
			pripremljeniIskaz.setInt(2, 1);
			pripremljeniIskaz.executeUpdate();
			updateLog(korisnik,"update on ehvan_table",new Timestamp((new Date()).getTime()));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	// delete

	public void pripremiDeleteEnisTable() {
		String sql ="DELETE FROM enis_table WHERE id = ?;";
		try {
			pripremljeniIskaz = konekcija.prepareStatement(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void deleteEnisTable() {
		try {
			pripremljeniIskaz.clearParameters();
			pripremljeniIskaz.setInt(1, 2);
			pripremljeniIskaz.executeUpdate();
			updateLog(korisnik,"delete on enis_table",new Timestamp((new Date()).getTime()));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	public void pripremiDeleteHamzaTable() {
		String sql ="DELETE FROM hamza_table WHERE id = ?;";
		try {
			pripremljeniIskaz = konekcija.prepareStatement(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void deleteHamzaTable() {
		try {
			pripremljeniIskaz.clearParameters();
			pripremljeniIskaz.setInt(1, 2);
			pripremljeniIskaz.executeUpdate();
			updateLog(korisnik,"delete on hamza_table",new Timestamp((new Date()).getTime()));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	public void pripremiDeleteEhvanTable() {
		String sql ="DELETE FROM ehvan_table WHERE id = ?;";
		try {
			pripremljeniIskaz = konekcija.prepareStatement(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void deleteEhvanTable() {
		try {
			pripremljeniIskaz.clearParameters();
			pripremljeniIskaz.setInt(1, 2);
			pripremljeniIskaz.executeUpdate();
			updateLog(korisnik,"delete on ehvan_table",new Timestamp((new Date()).getTime()));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public ArrayList<Task> getTasks(String tabela) {
		ArrayList<Task> tasks = new ArrayList<Task>();
		String sql = "SELECT id,task,comment FROM "+ tabela;
		try {
			rezultat = iskaz.executeQuery(sql);
			while(rezultat.next()){
	        	Integer id = rezultat.getInt("id");
	        	String task = rezultat.getString("task");
	        	String comment = rezultat.getString("comment");
	        	tasks.add(new Task(id,task,comment));
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tasks;
	}

	public ArrayList<Log> getLogs() {
		ArrayList<Log> logs = new ArrayList<Log>();
		String sql = "SELECT id,user_name,action_type,time_stamp FROM logs";
		try {
			rezultat = iskaz.executeQuery(sql);
			while(rezultat.next()){
	        	Integer id = rezultat.getInt("id");
	        	String user_name = rezultat.getString("user_name");
	        	String action_type = rezultat.getString("action_type");
	        	Timestamp time_stamp = rezultat.getTimestamp("time_stamp");
	        	logs.add(new Log(id,user_name,action_type,time_stamp));
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return logs;
	}

}