package com.example.sharecodeeditor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.context.annotation.*;

@Configuration
@EnableMongoRepositories(basePackages={"com.example.sharecodeeditor.mongorepository"})
@EnableJpaRepositories(basePackages={"com.example.sharecodeeditor.repository"})
@SpringBootApplication
public class SharecodeeditorApplication {

	public static void main(String[] args) {
		SpringApplication.run(SharecodeeditorApplication.class, args);
	}
}
