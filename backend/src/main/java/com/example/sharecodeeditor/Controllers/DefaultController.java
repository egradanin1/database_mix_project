package com.example.sharecodeeditor.Controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.sharecodeeditor.konekcije.PostgresKonekcija;
import com.example.sharecodeeditor.konekcije.MySqlKonekcija;

import com.example.sharecodeeditor.model.Task;
import com.example.sharecodeeditor.model.Log;
import com.example.sharecodeeditor.model.UserData;
import com.example.sharecodeeditor.model.Settings;
import com.example.sharecodeeditor.model.Friends;
import com.example.sharecodeeditor.model.Image;
import com.example.sharecodeeditor.model.Text;
import com.example.sharecodeeditor.model.Pdf;
import com.example.sharecodeeditor.model.UserUpload;


import com.example.sharecodeeditor.repository.UserDataRepository;
import com.example.sharecodeeditor.repository.SettingsRepository;
import com.example.sharecodeeditor.repository.FriendsRepository;
import com.example.sharecodeeditor.repository.UserUploadRepository;

import com.example.sharecodeeditor.service.UserService;
import com.example.sharecodeeditor.service.SettingsService;
import com.example.sharecodeeditor.service.FriendsService;
import com.example.sharecodeeditor.service.UserUploadService;
import com.example.sharecodeeditor.service.ImageMongoService;
import com.example.sharecodeeditor.service.TextMongoService;
import com.example.sharecodeeditor.service.PdfMongoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.http.MediaType;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.UUID;
import java.util.Optional;
import org.springframework.web.multipart.MultipartFile;
import org.bson.types.*;
import org.bson.BsonBinarySubType;
import java.nio.file.Path;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.json.*;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.node.*;
import java.lang.Iterable;
import org.bson.types.Binary;
import java.lang.Exception;


/** Represents an RestController.
 * @author Ehvan Gradanin i Enis Ahmetovic i Hamza Iseric
*/

@RestController
public class DefaultController {

    PostgresKonekcija db = new PostgresKonekcija();
    MySqlKonekcija mydb = new MySqlKonekcija();
    String database = "oracle&postgres";

    @Autowired
    /** Represents the UserService.
    */
    private UserService userService;

    @Autowired
    /** Represents the SettingsService.
    */
    private SettingsService settingsService;

    @Autowired
    /** Represents the FriendsService.
    */
    private FriendsService friendsService;


    @Autowired
    /** Represents the UserUploadService.
    */
    private UserUploadService userUploadService;

    @Autowired
     /** Represents the ImageMongoService.
    */
    private ImageMongoService imageMongoService;

    @Autowired
     /** Represents the TextMongoService.
    */
    private TextMongoService textMongoService;

    @Autowired
     /** Represents the PdfMongoService.
    */
    private PdfMongoService pdfMongoService;

     /** Represents the Logger.
    */
    private static final Logger logger = LoggerFactory.getLogger(DefaultController.class);

    @Autowired
    private UserDataRepository userDataRepository;

    @Autowired
    private UserUploadRepository userUploadRepository;


    @RequestMapping(value="/changeDatabase", method = RequestMethod.GET)
    public String changeDatabase() { 
        if(database.equals("mysql")){
            database = "oracle&postgres";
        }else{
            database = "mysql";
        }
        return database;
    }
    


    /**
     * REST GET method for getting a user by id using UserService method getUserById.
     *
     * @param  id Long id
     * @return Optional UserData
     */
   /* @RequestMapping(value="/user/{id}", method = RequestMethod.GET)
    public Optional<UserData> getUserWithId (@PathVariable Long id) {
         return userService.getUserById(id);
        
    }*/

    /**
     * REST GET method for getting a user by username using UserService method getUserByUsername.
     *
     * @param  username String username
     * @return UserData
     */
    @RequestMapping(value="/user/{username}", method = RequestMethod.GET)
    public UserData getUserWithUsername (@PathVariable String username) {
        if(database.equals("mysql")){
            return mydb.getUserWithUsername(username);
        }
        return userService.getUserByUsername(username);
        
    }

    /**
     * REST GET method for getting all userdatas using UserService method getAllUsers.
     *
     * @return list of userdatas
     */
    @RequestMapping(value="/users", method = RequestMethod.GET)
    public List<UserData> getUsers() { 
        if(database.equals("mysql")){
            return mydb.getAllUsers();
        }
        return userService.getAllUsers();
    }
    /**
     * REST POST method for saving a user in oracle database using UserService method save.
     *
     * @param  userData requestBody UserData POJO class userData
     */
    @RequestMapping(value = "/storeUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public UserData storeUser(@RequestBody UserData userData) {
        Long id = userService.getAllUsers().isEmpty() ? 1l : Long.valueOf(userService.getAllUsers().size() + 1);
        userData.setId(id);
        if(database.equals("mysql")){
            mydb.createUser(userData.getId(),userData.getFirstName(),userData.getLastName(),userData.getUsername(),userData.getPassword(),userData.getEmail());
        }else{
            userService.save(userData);
        }
        return userData;
    }
     /**
     * REST GET method for getting all images using  ImageMongoService method getAllImages.
     *
     * @return list of images
     */
    @RequestMapping(value="/images", method = RequestMethod.GET)
    public List<Image> getImages() { 
        return imageMongoService.getAllImages();
    }
    /**
     * REST POST method for saving a image in mongo database using  ImageMongoService method save.
     *
     * @param  multipart RequestParam("file") MultipartFile multipart
     * @param  userId RequestParam("userId") Long userId
     * @return  String success or failure
     */
    @RequestMapping(value = "/uploadImage", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String uploadImage(@RequestParam("file") MultipartFile multipart, @RequestParam("userId") Long userId ) {
        try {
            Image image = new Image();
            image.setId(UUID.randomUUID());
            image.setUserId(userId);
            image.setFile(new Binary(BsonBinarySubType.BINARY, multipart.getBytes()));
            imageMongoService.save(image);
        } catch (Exception e) {
            e.printStackTrace();
            return "failure";
        }
        return "success";
    }
     /**
     * REST GET method for getting an avatar image for user and saving it to local resources folder using FileOutputStream  method write.
     *
     * @param id PathVariable Long id
     * @return image containing user id passed as parameter
     */
    @RequestMapping(value="/getAvatarImage/{id}", method = RequestMethod.GET)
    public Image getAvatarImage(@PathVariable Long id){
        Image image = imageMongoService.findByUserId(id);
        Optional<UserData> userData = userService.getUserById(id);
        UserData user  = userData.get();
        String saveToPath = System.getProperty("user.dir")+"/src/main/resources/";
        
        Binary file = image.getFile();
        if(file != null && user != null) {
            FileOutputStream fileOuputStream = null;
            try {
                fileOuputStream = new FileOutputStream( saveToPath + user.getUsername() + "_avatar.jpg");
                fileOuputStream.write(file.getData());
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (fileOuputStream != null) {
                    try {
                        fileOuputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                        
                    }
                }
            }
        }
        return image;
    }
    
     /**
     * REST GET method for getting all texts using  TextMongoService method getAllTexts.
     *
     * @return list of texts
     */
    @RequestMapping(value="/texts", method = RequestMethod.GET)
    public List<Text> getTexts() { 
        return textMongoService.getAllTexts();
    }
    /**
     * REST POST method for saving a text in mongo database using  TextMongoService method save.
     *
     * @param  multipart RequestParam("file") MultipartFile multipart
     * @param  userId RequestParam("userId") Long userId
     * @return  String success or failure
     */
    @RequestMapping(value = "/uploadText", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String uploadText(@RequestParam("file") MultipartFile multipart, @RequestParam("userId") Long userId ) {
        try {
            Text text = new Text();
            text.setId(UUID.randomUUID());
            text.setUserId(userId);
            text.setFile(new Binary(BsonBinarySubType.BINARY, multipart.getBytes()));
            textMongoService.save(text);
        } catch (Exception e) {
            e.printStackTrace();
            return "failure";
        }
        return "success";
    }

    
     /**
     * REST GET method for getting all pdfs using  PdfMongoService method getAllPdfs.
     *
     * @return list of pdfs
     */
    @RequestMapping(value="/pdfs", method = RequestMethod.GET)
    public List<Pdf> getPdfs() { 
        return pdfMongoService.getAllPdfs();
    }
    /**
     * REST POST method for saving a pdf in mongo database using  PdfMongoService method save.
     *
     * @param  multipart RequestParam("file") MultipartFile multipart
     * @param  userId RequestParam("userId") Long userId
     * @return  String success or failure
     */
    @RequestMapping(value = "/uploadPdf", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String uploadPdf(@RequestParam("file") MultipartFile multipart, @RequestParam("userId") Long userId ) {
        try {
            Pdf pdf = new Pdf();
            pdf.setId(UUID.randomUUID());
            pdf.setUserId(userId);
            pdf.setFile(new Binary(BsonBinarySubType.BINARY, multipart.getBytes()));
            pdfMongoService.save(pdf);
        } catch (Exception e) {
            e.printStackTrace();
            return "failure";
        }
        return "success";
    }



     /**
     * REST GET method for getting all UserUploads using  UserUploadService method getAllUserUploads.
     *
     * @return list of UserUploads
     */
    @RequestMapping(value="/userUploads", method = RequestMethod.GET)
    public List<UserUpload> getUserUploads() { 
        if(database.equals("mysql")){
            return mydb.getAllUserUploads();
        }
        return userUploadService.getAllUserUploads();
    }

    /**
     * REST GET method for getting a userUpload by naziv using userUploadService method getUserUploadByNaziv.
     *
     * @param  naziv String naziv
     * @return Optional UserUpload
     */
    @RequestMapping(value="/userUpload/{naziv}", method = RequestMethod.GET)
    public UserUpload getUserUploadByNaziv (@PathVariable String naziv) {
        if(database.equals("mysql")){
            return mydb.getUserUploadByNaziv(naziv);
        }
         return userUploadService.getUserUploadByNaziv(naziv);
        
    }




    /**
     * REST POST method for saving a UserUpload in oracle database using  UserUploadService method save.
     *
     * @param  multipart RequestParam("file") MultipartFile multipart
     * @param  userId RequestParam("userId") Long userId
     * @return  String success or failure
     */
    @RequestMapping(value = "/uploadUserUpload", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = "multipart/form-data")
    public String uploadUserUpload(@RequestParam("file") MultipartFile multipart, @RequestParam("userId") Long userId ) {
        try {
            if(database.equals("mysql")){
                UserUpload upload = mydb.getUserUploadByNaziv(multipart.getOriginalFilename());
                if(upload != null && userId == upload.getUser().getId()){
                    return "greska!";
                }
                Long id = mydb.getAllUserUploads().isEmpty() ? 1l : Long.valueOf(mydb.getAllUserUploads().size() + 1);
                mydb.createUserUpload(id,multipart.getOriginalFilename(),multipart.getContentType(),"neki_url",new Binary(BsonBinarySubType.BINARY, multipart.getBytes()),userId);
            }else{
                UserUpload upload = userUploadService.getUserUploadByNaziv(multipart.getOriginalFilename());
                if(upload != null && userId == upload.getUser().getId()){
                    return "greska!";
                }
                UserUpload userUpload = new UserUpload();
                // privremeno dok ne skontam sekvencu
                Long id = userUploadService.getAllUserUploads().isEmpty() ? 1l : Long.valueOf(userUploadService.getAllUserUploads().size() + 1);
                userUpload.setId(id);
                userUpload.setNaziv(multipart.getOriginalFilename());
                userUpload.setType(multipart.getContentType());
                userUpload.setImage("neki_url");
                userUpload.setUser(userService.getUserById(userId).get());
                userUpload.setData(new Binary(BsonBinarySubType.BINARY, multipart.getBytes()));
                userUploadService.save(userUpload);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "failure";
        }
        return "success";
    }






    @RequestMapping(value="/openConnection", method = RequestMethod.GET)
    public void openConnection() {
        db.loadDriver();
        db.connect();
        mydb.loadDriver();
        mydb.connect();
    }
    

    // insert

    @RequestMapping(value="/insertToEnisTable", method = RequestMethod.GET)
    public void insertEnis() {
        if(database.equals("mysql")){
            mydb.pripremiInsertToEnisTable();
            mydb.insertToEnisTable();
        }else{
            db.pripremiInsertToEnisTable();
            db.insertToEnisTable();
        }
       
 
    }
    @RequestMapping(value="/insertToHamzaTable", method = RequestMethod.GET)
    public void insertHamza() {
        if(database.equals("mysql")){
            mydb.pripremiInsertToHamzaTable();
            mydb.insertToHamzaTable();
        }else{
            db.pripremiInsertToHamzaTable();
            db.insertToHamzaTable();
        }
        
 
    }
    @RequestMapping(value="/insertToEhvanTable", method = RequestMethod.GET)
    public void insertEhvan() {
        if(database.equals("mysql")){
            mydb.pripremiInsertToEhvanTable();
            mydb.insertToEhvanTable();
        }else{
            db.pripremiInsertToEhvanTable();
            db.insertToEhvanTable();
        }
    
    }

    // update

    @RequestMapping(value="/updateEnisTable", method = RequestMethod.GET)
    public void updateEnis() {
        if(database.equals("mysql")){
            mydb.pripremiUpdateEnisTable();
            mydb.updateEnisTable();
        }else{
            db.pripremiUpdateEnisTable();
            db.updateEnisTable();
        }
       
 
    }
    @RequestMapping(value="/updateHamzaTable", method = RequestMethod.GET)
    public void updateHamza() {
        if(database.equals("mysql")){
            mydb.pripremiUpdateHamzaTable();
            mydb.updateHamzaTable();
        }else{
            db.pripremiUpdateHamzaTable();
            db.updateHamzaTable();
        }
        
 
    }
    @RequestMapping(value="/updateEhvanTable", method = RequestMethod.GET)
    public void updateEhvan() {
        if(database.equals("mysql")){
            mydb.pripremiUpdateEhvanTable();
            mydb.updateEhvanTable();
        }else{
            db.pripremiUpdateEhvanTable();
            db.updateEhvanTable();
        }
        
 
    }


    // delete

    @RequestMapping(value="/deleteEnisTable", method = RequestMethod.GET)
    public void deleteEnis() {
        if(database.equals("mysql")){
            mydb.pripremiDeleteEnisTable();
            mydb.deleteEnisTable();
        }else{
            db.pripremiDeleteEnisTable();
            db.deleteEnisTable();
        }
        
 
    }
    @RequestMapping(value="/deleteHamzaTable", method = RequestMethod.GET)
    public void deleteHamza() {
        if(database.equals("mysql")){
            mydb.pripremiDeleteHamzaTable();
            mydb.deleteHamzaTable();
        }else{
            db.pripremiDeleteHamzaTable();
            db.deleteHamzaTable();
        }
       
 
    }
    @RequestMapping(value="/deleteEhvanTable", method = RequestMethod.GET)
    public void deleteEhvan() {
        if(database.equals("mysql")){
            mydb.pripremiDeleteEhvanTable();
            mydb.deleteEhvanTable();
        }else{
            db.pripremiDeleteEhvanTable();
            db.deleteEhvanTable();
        }
        
 
    }

    @RequestMapping(value="/tasks/{tabela}", method = RequestMethod.GET)
    public ArrayList<Task> getTasks(@PathVariable String tabela) {
        if(database.equals("mysql")){
            mydb.getStatement();
            return mydb.getTasks(tabela);
        }else{
            db.getStatement();
        }
        
        return db.getTasks(tabela);
    }

    @RequestMapping(value="/logs", method = RequestMethod.GET)
    public ArrayList<Log> getLogs() {
        if(database.equals("mysql")){
            mydb.getStatement();
            return mydb.getLogs();
        }else{
            db.getStatement();
        }
        
        return db.getLogs();
    }

    @RequestMapping(value="/closeConnection", method = RequestMethod.GET)
    public void closeConnection(){
        db.diskonekt();
        mydb.diskonekt();
    }


    @RequestMapping(value="/getMetaData", method = RequestMethod.GET)
    public JsonNode getMetaData(){
        return db.getMetaData();
    }

    @RequestMapping(value="/migrateDatabase", method = RequestMethod.GET)
    public void migrateDatabase(){
        try{

            mydb.setAutoCommit(false);
            JsonNode metadata = db.getMetaData();




            // 1. kreirati tabele
            String tableCreationSql = "";


           

            ArrayList<String> table_names = new ArrayList<String>();
            ArrayNode array = (ArrayNode)metadata.get("Info o tabelama");
            if (array.isArray()) {
                for (final JsonNode objNode : array) {
                    JsonNode node = objNode.get("Ime Tabela");
                    table_names.add(node.asText(""));

                }
            }

            ObjectNode kolone = (ObjectNode)metadata.get("Kolone");
            for (String table_name : table_names) {
                tableCreationSql = "";
                ArrayNode table = (ArrayNode)kolone.get(table_name);
                tableCreationSql = "Create Table "+ table_name + " (";
                for (final JsonNode kolona : table) {
                    String imeKolone = kolona.get("Ime Kolone").asText("");
                    String tipKolone = kolona.get("Tip Kolone").asText("");
                    if (tipKolone.equals("bpchar")){
                        tipKolone = "varchar";
                    }
                    if (tipKolone.equals("serial")){
                        tipKolone = "int";
                    }
                    int velicinaKolone = kolona.get("Velicina Kolone").asInt();
                    if (imeKolone.equals("id")){
                        tableCreationSql += "id int(" + velicinaKolone + ") auto_increment not null PRIMARY KEY,";   
                    }
                    else if(tipKolone.equals("timestamp")){

                        tipKolone = "timestamp";
                        tableCreationSql += imeKolone + " " + tipKolone + ",";
                    }
                    else{
                        tableCreationSql += imeKolone + " " + tipKolone + "(" + velicinaKolone + "),";
                    }


                }
                tableCreationSql = tableCreationSql.substring(0, tableCreationSql.length() - 1);
                tableCreationSql += ")";
                mydb.executeSql(tableCreationSql);

            }

            // creating users and adding privileges

            ArrayList<String> users = db.getDatabaseUsers();
            ArrayList<String> mysqlUsers = mydb.getDatabaseUsers();
            for (String mysqlUser : mysqlUsers){
                users.remove(mysqlUser);
            }
            users.remove("postgres");
            if(!users.isEmpty()){
                for (String username : users){
                    mydb.createNewDatabaseUser(username);
                }
                for (String table_name : table_names) {
                    try {
                        ResultSet rezultat = db.getUsersPrivileges(table_name);
                        while(rezultat.next()){
                            if(!rezultat.getString("grantee").equals("postgres") && !rezultat.getString("privilege_type").equals("TRUNCATE")){
                                mydb.addPrivileges(rezultat.getString("grantee"),rezultat.getString("privilege_type"),table_name);
                            }
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                
                }
            }
            
           

            
           


            // punjenje tabela

            for (String table_name : table_names){
                ArrayList<Task> tasks = new ArrayList<Task>();
                ArrayList<Log> logs =  new ArrayList<Log>();
                db.getStatement();
                if (!table_name.equals("logs")){
                    tasks = db.getTasks(table_name);
                    for (Task task : tasks){
                        mydb.createTask(table_name,task);
                    }
                }else{
                    logs = db.getLogs();
                    for(Log log : logs){
                        mydb.createLog(table_name,log);
                    }
                }
            }

            // triggeri

            Set set=db.getAllTriggers().entrySet();
            Iterator itr=set.iterator();  
            while(itr.hasNext()){  
                Map.Entry entry=(Map.Entry)itr.next();  
                String triggerBody= db.getTriggerBody((String)entry.getKey());
                System.out.println(triggerBody);  
            }  

            db.getTriggerProcedure();

          // migracija mongo tabela

           String sql = "CREATE TABLE images( id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,userId BIGINT(19) NOT NULL, file LONGBLOB NOT NULL);";
           mydb.executeSql(sql);
           List<Image> images = imageMongoService.getAllImages();
           if(!images.isEmpty()){
                for (Image image : images){
                    mydb.createImage(image.getUserId(),image.getFile());
                }
           }

           sql = "CREATE TABLE user_data( id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,firstname varchar(50) NOT NULL, lastname varchar(50) NOT NULL,username varchar(50) NOT NULL,password varchar(50) NOT NULL,email varchar(50) NOT NULL);";
           mydb.executeSql(sql);
           List<UserData> usersData = userService.getAllUsers();
           if(!usersData.isEmpty()){
                for (UserData user : usersData){
                    mydb.createUser(user.getId(),user.getFirstName(),user.getLastName(),user.getUsername(),user.getPassword(),user.getEmail());
                }
           }

           sql = "CREATE TABLE user_upload( id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,naziv varchar(150) NOT NULL, type varchar(50) NOT NULL,image varchar(50) NOT NULL,data LONGBLOB NOT NULL,user INT(6) UNSIGNED NOT NULL, FOREIGN KEY (user) REFERENCES user_data(id));";
           mydb.executeSql(sql);
           List<UserUpload> usersUpload = userUploadService.getAllUserUploads();
           if(!usersUpload.isEmpty()){
                for (UserUpload userUpload : usersUpload){
                    mydb.createUserUpload(userUpload.getId(),userUpload.getNaziv(),userUpload.getType(),userUpload.getImage(),userUpload.getData(),userUpload.getUser().getId());
                }
           }
        
        }catch(Exception e){
            mydb.rollback();
        }
        mydb.commit();

        
    }
    @RequestMapping(value="/dropData", method = RequestMethod.GET)
    public void dropData(){
        List<UserUpload> usersUploads = userUploadRepository.findAll();
        for(UserUpload userUpload :  usersUploads){
            userUploadService.delete(userUpload);
        }
        List<UserData> users = userDataRepository.findAll();
        for(UserData user : users){
            userService.delete(user);
        }
        
    }


}
