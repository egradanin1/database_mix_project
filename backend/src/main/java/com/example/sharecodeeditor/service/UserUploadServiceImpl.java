package com.example.sharecodeeditor.service;


import com.example.sharecodeeditor.model.UserUpload;
import com.example.sharecodeeditor.repository.UserUploadRepository;
import com.example.sharecodeeditor.service.UserUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Arrays;
import java.util.List;
import java.util.Collection;
import java.util.Optional;


/** Represents an UserUpload service interface implementation.
 * @author Ehvan Gradanin
*/

@Service
public class UserUploadServiceImpl implements UserUploadService {
    @Autowired
    /** Represents the UserUploadDataRepository.
    */
    private UserUploadRepository userUploadRepository;

    @Override
    /**
     * Returns Optional UserUpload with the id using UserUploadRepository method findById.
     * @param  id id of UserUpload that we are searching for
     * @return Optional UserUpload found by id
     */
    public Optional<UserUpload> getUserUploadById(Long id) {
        return userUploadRepository.findById(id);
    }

    @Override
    /**
     * Returns UserUpload object after saving it to oracle database using UserUploadRepository method save.
     *
     * @param  userUpload POJO UserUpload 
     * @return saved UserUpload
     */
    public UserUpload save(UserUpload userUpload) {
        return userUploadRepository.save(userUpload);
    }

    @Override
    /**
     * Returns list of all UserUploads in oracle datatabase using UserUploadRepository method findAll.
     * @return list of UserUploads
     */
    public List<UserUpload> getAllUserUploads() {
        return userUploadRepository.findAll();
    }

    @Override
    /**
     * Returns Optional UserUpload with the naziv using UserUploadRepository method findByNaziv.
     * @param  naziv naziv of UserUpload that we are searching for
     * @return Optional UserUpload found by naziv
     */
    public UserUpload getUserUploadByNaziv(String naziv) {
        return userUploadRepository.findByNaziv(naziv);
    }
    @Override
    public void delete(UserUpload userUpload){
        userUploadRepository.delete(userUpload);
    }
}
