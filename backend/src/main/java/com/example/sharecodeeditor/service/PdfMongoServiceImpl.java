package com.example.sharecodeeditor.service;

import com.example.sharecodeeditor.model.Pdf;
import com.example.sharecodeeditor.mongorepository.PdfRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.HashSet;
import java.util.Arrays;
import java.util.List;

/** Represents an pdf mongo service interface implementation.
 * @author Hamza Iseric
*/

@Service
public class PdfMongoServiceImpl implements PdfMongoService {
    @Autowired
    /** Represents the PdfRepository.
    */
    private PdfRepository pdfRepository;


    @Override
    /**
     * Calls pdfRepository to save pdf to mongo database.
     *
     * @param  pdf POJO Pdf 
     */
    public void save(Pdf pdf) {
        pdfRepository.save(pdf);
    }

    @Override
    /**
     * Returns list of all pdfs in datatabase using pdfRepository method findAll.
     * @return list of pdfs
     */
    public List<Pdf> getAllPdfs() {
        return pdfRepository.findAll();
    }

    @Override
    /**
     * Returns Pdf that belongs to user with the userId.
     * @param  userId id of user that we are searching pdf for
     * @return pdf found by userId
     */
    public Pdf findByUserId(Long userId){
        return pdfRepository.findByUserId(userId);
    }
}
