package com.example.sharecodeeditor.service;


import com.example.sharecodeeditor.model.Settings;
import com.example.sharecodeeditor.repository.SettingsRepository;
import com.example.sharecodeeditor.service.SettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Arrays;
import java.util.List;
import java.util.Collection;

@Service
public class SettingsServiceImpl implements SettingsService {
    @Autowired
    private SettingsRepository settingsRepository;


    @Override
    public void save(Settings settings) {
        settingsRepository.save(settings);
    }

   /* @Override
    public Collection<Settings> getAllSettings() {
        return settingsRepository.findAll();
    }*/
}
