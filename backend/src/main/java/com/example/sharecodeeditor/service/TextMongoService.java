package com.example.sharecodeeditor.service;

import com.example.sharecodeeditor.model.Text;
import java.util.List;
import java.util.Collection;


/** Represents an text mongo service interface.
 * @author Enis Ahmetovic
*/

public interface TextMongoService {
    /** 
     * Saves text to database.
     *
     * @param  text POJO Text 
     */
    void save(Text text);
    /**
     * Returns list of all texts in datatabase.
     * @return list of texts
     */
    List<Text> getAllTexts();
    /**
     * Returns Text that belongs to user with the userId.
     * @param  userId id of user that we are searching text for
     * @return text found by userId
     */
    Text findByUserId(Long userId);
}
