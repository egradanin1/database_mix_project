package com.example.sharecodeeditor.service;

import com.example.sharecodeeditor.model.Text;
import com.example.sharecodeeditor.mongorepository.TextRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.HashSet;
import java.util.Arrays;
import java.util.List;

/** Represents an text mongo service interface implementation.
 * @author Enis Ahmetovic
*/

@Service
public class TextMongoServiceImpl implements TextMongoService {
    @Autowired
    /** Represents the TextRepository.
    */
    private TextRepository textRepository;


    @Override
    /**
     * Calls textRepository to save text to mongo database.
     *
     * @param  text POJO Text 
     */
    public void save(Text text) {
        textRepository.save(text);
    }

    @Override
    /**
     * Returns list of all texts in datatabase using textRepository method findAll.
     * @return list of texts
     */
    public List<Text> getAllTexts() {
        return textRepository.findAll();
    }

    @Override
    /**
     * Returns Text that belongs to user with the userId.
     * @param  userId id of user that we are searching text for
     * @return text found by userId
     */
    public Text findByUserId(Long userId){
        return textRepository.findByUserId(userId);
    }
}
