package com.example.sharecodeeditor.service;


import com.example.sharecodeeditor.model.Friends;
import com.example.sharecodeeditor.repository.FriendsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Arrays;
import java.util.List;
import java.util.Collection;

@Service
public class FriendsServiceImpl implements FriendsService {
    @Autowired
    private FriendsRepository friendsRepository;


    @Override
    public void save(Friends friends) {
        friendsRepository.save(friends);
    }

    /*@Override
    public Collection<Friends> getAllFriends() {
        return friendsRepository.findAll();
    }*/
}
