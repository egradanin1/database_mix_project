package com.example.sharecodeeditor.service;

import com.example.sharecodeeditor.model.UserData;
import java.util.List;
import java.util.Collection;
import java.util.Optional;


/** Represents an user service interface.
 * @author Ehvan Gradanin
*/

public interface UserService {
	/**
     * Returns Optional UserData with the id.
     * @param  id id of user that we are searching for
     * @return Optional UserData found by id
     */
	Optional<UserData> getUserById(Long id);
	/**
	 * Returns UserData object after saving it to oracle database.
	 *
	 * @param  user POJO UserData 
	 * @return saved userdata
	 */
    UserData save(UserData user);
    /**
	 * Returns list of all userdatas in oracle datatabase.
	 * @return list of userdatas
	 */
   	List<UserData> getAllUsers();

   	/**
     * Returns UserData with the username.
     * @param  username username of user that we are searching for
     * @return UserData found by username
     */
	UserData getUserByUsername(String username);
	void delete(UserData userData);
}
