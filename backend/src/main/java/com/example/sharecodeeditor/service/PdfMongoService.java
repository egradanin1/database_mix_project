package com.example.sharecodeeditor.service;

import com.example.sharecodeeditor.model.Pdf;
import java.util.List;
import java.util.Collection;


/** Represents an pdf mongo service interface.
 * @author Hamza Iseric
*/

public interface PdfMongoService {
    /** 
     * Saves pdf to database.
     *
     * @param  pdf POJO Pdf 
     */
    void save(Pdf pdf);
    /**
     * Returns list of all pdfs in datatabase.
     * @return list of pdfs
     */
    List<Pdf> getAllPdfs();
    /**
     * Returns Pdf that belongs to user with the userId.
     * @param  userId id of user that we are searching pdf for
     * @return pdf found by userId
     */
    Pdf findByUserId(Long userId);
}
