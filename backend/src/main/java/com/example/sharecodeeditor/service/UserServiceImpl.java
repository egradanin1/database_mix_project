package com.example.sharecodeeditor.service;


import com.example.sharecodeeditor.model.UserData;
import com.example.sharecodeeditor.repository.UserDataRepository;
import com.example.sharecodeeditor.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Arrays;
import java.util.List;
import java.util.Collection;
import java.util.Optional;


/** Represents an user service interface implementation.
 * @author Ehvan Gradanin
*/

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    /** Represents the UserDataRepository.
    */
    private UserDataRepository userDataRepository;

    @Override
    /**
     * Returns Optional UserData with the id using userDataRepository method findById.
     * @param  id id of user that we are searching for
     * @return Optional UserData found by id
     */
    public Optional<UserData> getUserById(Long id) {
        return userDataRepository.findById(id);
    }

    @Override
    /**
     * Returns UserData object after saving it to oracle database using userDataRepository method save.
     *
     * @param  user POJO UserData 
     * @return saved userdata
     */
    public UserData save(UserData user) {
        return userDataRepository.save(user);
    }

    @Override
    /**
     * Returns list of all userdatas in oracle datatabase using userDataRepository method findAll.
     * @return list of userdatas
     */
    public List<UserData> getAllUsers() {
        return userDataRepository.findAll();
    }

    @Override
    /**
     * Returns UserData with the username using userDataRepository method findByUsername.
     * @param  username username of user that we are searching for
     * @return UserData found by username
     */
    public UserData getUserByUsername(String username) {
        return userDataRepository.findByUsername(username);
    }
    public void delete(UserData userData){
        userDataRepository.delete(userData);
    }
}
