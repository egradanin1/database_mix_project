package com.example.sharecodeeditor.service;

import com.example.sharecodeeditor.model.UserUpload;
import java.util.List;
import java.util.Collection;
import java.util.Optional;


/** Represents an UserUpload service interface.
 * @author Ehvan Gradanin
*/

public interface UserUploadService {
	/**
     * Returns Optional UserUpload with the id.
     * @param  id id of UserUpload that we are searching for
     * @return Optional UserUpload found by id
     */
	Optional<UserUpload> getUserUploadById(Long id);
	/**
	 * Returns UserUpload object after saving it to oracle database.
	 *
	 * @param  userUpload POJO UserUpload
	 * @return saved UserUpload
	 */
    UserUpload save(UserUpload userUpload);
    /**
	 * Returns list of all UserUploads in oracle datatabase.
	 * @return list of UserUploads
	 */
   	List<UserUpload> getAllUserUploads();

   	/**
     * Returns Optional UserUpload with the naziv.
     * @param  naziv naziv of UserUpload that we are searching for
     * @return Optional UserUpload found by naziv
     */
	UserUpload getUserUploadByNaziv(String naziv);
	void delete(UserUpload userUpload);
}
