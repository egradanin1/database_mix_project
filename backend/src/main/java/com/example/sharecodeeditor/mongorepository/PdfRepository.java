package com.example.sharecodeeditor.mongorepository;

import java.util.List;
import java.util.UUID;
import com.example.sharecodeeditor.model.Pdf;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


/** Represents an pdf mongo repository.
 * @author Hamza Iseric
*/

@Repository
public interface PdfRepository extends MongoRepository<Pdf, String> {
	/**
	 * Returns Pdf object after saving it to mongo database.
	 *
	 * @param  pdf POJO Pdf 
	 * @return saved pdf
	 */
    public Pdf insert(Pdf pdf);
    /**
	 * Returns list of all pdfs in datatabase.
	 * @return list of pdfs
	 */
    public List<Pdf> findAll();
    /**
	 * Returns Pdf that belongs to user with the userId.
	 * @param  userId id of user that we are searching pdf for
	 * @return pdf found by userId
	 */
    public Pdf findByUserId(Long userId);

}