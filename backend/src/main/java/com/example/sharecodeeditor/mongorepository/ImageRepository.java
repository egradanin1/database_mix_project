package com.example.sharecodeeditor.mongorepository;

import java.util.List;
import java.util.UUID;
import com.example.sharecodeeditor.model.Image;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


/** Represents an image mongo repository.
 * @author Ehvan Gradanin
*/

@Repository
public interface ImageRepository extends MongoRepository<Image, String> {
	/**
	 * Returns Image object after saving it to mongo database.
	 *
	 * @param  image POJO Image 
	 * @return saved image
	 */
    public Image insert(Image image);
    /**
	 * Returns list of all images in datatabase.
	 * @return list of images
	 */
    public List<Image> findAll();
    /**
	 * Returns Image that belongs to user with the userId.
	 * @param  userId id of user that we are searching image for
	 * @return image found by userId
	 */
    public Image findByUserId(Long userId);

}