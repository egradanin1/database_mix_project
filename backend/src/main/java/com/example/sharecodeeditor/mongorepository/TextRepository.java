package com.example.sharecodeeditor.mongorepository;

import java.util.List;
import java.util.UUID;
import com.example.sharecodeeditor.model.Text;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


/** Represents an text mongo repository.
 * @author Enis Ahmetovic
*/

@Repository
public interface TextRepository extends MongoRepository<Text, String> {
	/**
	 * Returns Text object after saving it to mongo database.
	 *
	 * @param  text POJO Text 
	 * @return saved text
	 */
    public Text insert(Text text);
    /**
	 * Returns list of all texts in datatabase.
	 * @return list of texts
	 */
    public List<Text> findAll();
    /**
	 * Returns Text that belongs to user with the userId.
	 * @param  userId id of user that we are searching text for
	 * @return text found by userId
	 */
    public Text findByUserId(Long userId);

}