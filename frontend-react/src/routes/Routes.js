import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import App from '../App';
import Dashboard from '../components/dashboard/Dashboard';
import Upload from '../components/upload/Upload';
import createBrowserHistory from 'history/createBrowserHistory';

const browserHistory = createBrowserHistory();

export default (
	<Router>
		<App>
			<Route path="/dashboard" component={Dashboard} />
			<Route path="/upload" component={Upload} />
			<Route exact={true} path="/" component={Upload} />
		</App>
	</Router>
);
