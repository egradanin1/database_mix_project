import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import './AttachmentCard.css';

class AttachmentCard extends Component {
	handleClick() {
		let { data, base64ToArrayBuffer, saveByteArray, naziv, type } = this.props;
		let sampleArr = base64ToArrayBuffer(data.data);
		saveByteArray(naziv, sampleArr, type);
	}
	render() {
		let images = {
			'application/pdf':
				'http://www.myiconfinder.com/uploads/iconsets/256-256-e2784aa40ee00e618ca327eb12dcc4ed.png',
			'image/jpeg': 'http://www.myiconfinder.com/uploads/iconsets/256-256-4e85033cfdb3da00ce01c67a4bc37cdf.png'
		};
		return (
			<div className="download-item">
				<a onClick={() => this.handleClick()}>
					<img alt={this.props.naziv} src={images[this.props.type]} />
					<div className="basic-info">
						<h3>{this.props.user.username}</h3>
						<h4>({this.props.naziv})</h4>
					</div>
				</a>
			</div>
		);
	}
}

export default AttachmentCard;
