import React, { Component } from 'react';
import './DatabaseOptions.css';
import axios from 'axios';

class DatabaseOptions extends Component {
	
	
	handleDatabaseChange = () => {
		axios
			.get('/changeDatabase')
			.then(response => {
				alert("Database switched to " + response.data);
			})
			.catch(error => console.log(error));
	}

	handleOpenConnection = () => {
		axios
			.get('/openConnection')
			.then(response => {
				alert("You are connected to mysql!");
			})
			.catch(error => console.log(error));
		
	}

	handleMigrateDatabase = () => {
		axios
			.get('/migrateDatabase')
			.then(response => {
				alert("Successful Data Migration!");
			})
			.catch(error => console.log(error));
		
	}

	render() {
		return (
			<div className="database-option-buttons">
				<button onClick={this.handleMigrateDatabase}>Migrate database</button>
				<button onClick={this.handleOpenConnection}>Connect to mysql</button>
				<button onClick={this.handleDatabaseChange}>Change database</button>
			</div>
		);
	}
}

export default DatabaseOptions;
