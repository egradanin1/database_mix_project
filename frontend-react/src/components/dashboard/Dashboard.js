import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import AttachmentCard from '../attachmentcard/AttachmentCard';
import './Dashboard.css';
import DatabaseOptions from  '../databaseoptions/DatabaseOptions.js';
import axios from 'axios';
import ReactLoading from 'react-loading';

class Dashboard extends Component {
	state = {
		attachments: [],
		filteredAttachments: [],
		filter: '',
		isLoaded: false
	};
	componentWillMount() {
		axios
			.get('/userUploads')
			.then(response => {
				console.log(response);
				this.setState({
					attachments: response.data,
					filteredAttachments: response.data,
					isLoaded: true
				});
			})
			.catch(error => console.log(error));
	}
	base64ToArrayBuffer(base64) {
		var binaryString = window.atob(base64);
		var binaryLen = binaryString.length;
		var bytes = new Uint8Array(binaryLen);
		for (var i = 0; i < binaryLen; i++) {
			var ascii = binaryString.charCodeAt(i);
			bytes[i] = ascii;
		}
		return bytes;
	}
	saveByteArray(reportName, byte, type) {
		var blob = new Blob([byte], { type });
		var link = document.createElement('a');
		link.href = window.URL.createObjectURL(blob);
		var fileName = reportName;
		link.download = fileName;
		link.click();
	}
	handleInput(e) {
		this.setState({
			filteredAttachments: this.state.attachments.filter(
				attachment => ~attachment.naziv.toLowerCase().indexOf(e.target.value.toLowerCase())
			),
			filter: e.target.value
		});
	}
	handleClick() {
		this.state.attachments.forEach(attachment =>
			this.saveByteArray(attachment.naziv, this.base64ToArrayBuffer(attachment.data.data), attachment.type)
		);
	}

	render() {
		let { attachments, filteredAttachments, filter, isLoaded } = this.state;
		return (
			<div>
				<DatabaseOptions/>
				<input
					type="text"
					value={filter}
					id="filter-input"
					onChange={e => this.handleInput(e)}
					placeholder="Search"
				/>
				<button className="download-button" onClick={() => this.handleClick()}>
					Download All
				</button>
				<ReactLoading
					hidden={isLoaded}
					type="spinningBubbles"
					color="#877ad2"
					height={200}
					width={200}
					className="spin"
				/>
				<div className="download-list">
					{filteredAttachments.map((attachment, index) => (
						<AttachmentCard
							{...attachment}
							base64ToArrayBuffer={base64 => {
								return this.base64ToArrayBuffer(base64);
							}}
							saveByteArray={(reportName, byte, type) => {
								this.saveByteArray(reportName, byte, type);
							}}
							key={attachment.id}
							id={index}
						/>
					))}
				</div>
			</div>
		);
	}
}

export default Dashboard;
