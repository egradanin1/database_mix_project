import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import './Upload.css';
import DatabaseOptions from  '../databaseoptions/DatabaseOptions.js';
import axios from 'axios';

class Upload extends Component {
	initialState = {
		username: '',
		password: '',
		firstname: '',
		lastname: '',
		email: '',
		file: null
	};
	state = this.initialState

	handleClick(e) {
		let { username, password, firstname, lastname, email, file } = this.state;
		let fd = new FormData();
		fd.append('file', file);

		axios
			.get(`/user/${username}`)
			.then(response =>
				response.data
					? axios
							.post('uploadUserUpload', !fd.append('userId', response.data.id) && fd, {
								headers: {
									'Content-Type': 'multipart/form-data'
								}
							})
							.then(response =>
								response.data == 'greska!' 
									? alert('Desila se greska! Isti file-ovi!')
									: alert("Uploaded!")
							)
							.catch(error => console.log(error))
					: axios
							.post('/storeUser', {
								username: username,
								password: password,
								firstName: firstname,
								lastName: lastname,
								email: email
							})
							.then(response =>
								response.data
									? axios
											.post('uploadUserUpload', !fd.append('userId', response.data.id) && fd, {
												headers: {
													'Content-Type': 'multipart/form-data'
												}
											})
											.then(response =>
												response.data == 'greska!'
													? alert('Desila se greska! Isti file-ovi!')
													: alert("Uploaded!")
											)
											.catch(error => console.log(error))
									: console.log('greska!')
							)
							.catch(error => console.log(error))
			)
			.catch(error => console.log(error));
		this.setState(this.initialState);
	}
	handleChange(uploadedFile) {
		this.setState({ file: uploadedFile[0] });
	}

	handleInput(e) {
		this.setState({
			[e.target.name]: e.target.value
		});
	}

	render() {
		return (
			<div>
				<DatabaseOptions/>
				<div className="App-upload">
					
					<h3>Upload Document!</h3>
					<input
						type="text"
						name="username"
						value={this.state.username}
						placeholder="Username"
						onChange={e => this.handleInput(e)}
					/>
					<br />
					<input
						type="text"
						name="firstname"
						value={this.state.firstname}
						placeholder="Ime"
						onChange={e => this.handleInput(e)}
					/>
					<br />
					<input
						type="text"
						name="lastname"
						value={this.state.lastname}
						placeholder="Prezime"
						onChange={e => this.handleInput(e)}
					/>
					<br />
					<input
						type="text"
						name="email"
						value={this.state.email}
						placeholder="Email"
						onChange={e => this.handleInput(e)}
					/>
					<br />
					<input
						type="password"
						name="password"
						value={this.state.password}
						placeholder="Password"
						onChange={e => this.handleInput(e)}
					/>
					<br />
					<input type="file" onChange={e => this.handleChange(e.target.files)} />
					<br />
					<button onClick={e => this.handleClick(e)}>Upload</button>
				</div>
			</div>
		);
	}
}

export default Upload;
