import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import './index.css';
import routes from './routes/Routes';
import { register } from './serviceWorker';

ReactDOM.render(<BrowserRouter>{routes}</BrowserRouter>, document.getElementById('root'));
register();
